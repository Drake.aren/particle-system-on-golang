package main

// Particle 123
type Particle struct {
	position       Vector3
	direction      Vector3
	lifeSpan       float32
	invInitialLife float32 // ???
	size           float32
	deep           float32 // ???
	angle          float32 // ???
	angleSpeed     float32 // ???
}

func (p *Particle) update(deltaTime float32) {
	p.direction.mult(deltaTime)
	p.direction.add(p.direction)
}

func (p *Particle) isDead(deltaTime float32) bool {
	if (p.lifeSpan - deltaTime) < 0.0 {
		return true
	}
	return false
}
