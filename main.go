package main

import (
	"log"
	"runtime"
	"time"

	"github.com/go-gl/gl/v4.1-core/gl" // OR: github.com/go-gl/gl/v2.1/gl
	"github.com/go-gl/glfw/v3.2/glfw"
)

const (
	width  = 800
	height = 800
)

func main() {
	runtime.LockOSThread()

	window := initGlfw()
	defer glfw.Terminate()

	// program := initOpenGL()

	ps := initParticleSystem()

	for !window.ShouldClose() {
		timestamp := float32(time.Now().UnixNano())
		gl.Clear(gl.COLOR_BUFFER_BIT)

		ps.update(timestamp)
		// draw(makeVao(square), window, program)

		glfw.PollEvents()
	}
}

func initParticleSystem() ParticleSystem {
	position := Vector3{0, 0, 0}
	var particleCredit float32 = 0
	var spawnRate float32 = 250
	var particleSpeedMin float32 = 150
	var particleSpeedMax float32 = 10
	var particlelifeMin float32 = 1.5
	var particlelifeMax float32 = 0.5
	directionalForce := Vector3{0, 100, 0}
	var frictionFactor float32 = 1
	var particleSizeMin float32 = 35
	var particleSizeMax float32 = 15
	rotate := true
	time := float32(time.Now().UnixNano())

	ps := ParticleSystem{[]Particle{}, particleCredit, position, []PointForce{}, directionalForce, spawnRate, particleSpeedMin, particleSpeedMax, particleSizeMin, particleSizeMax, particlelifeMin, particlelifeMax, frictionFactor, rotate, [12]Vector4{}, time}
	ps.setColorScheme("fire")

	return ps
}

// initGlfw initializes glfw and returns a Window to use.
func initGlfw() *glfw.Window {
	if err := glfw.Init(); err != nil {
		panic(err)
	}
	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	window, err := glfw.CreateWindow(width, height, "Golang test", nil, nil)

	if err != nil {
		panic(err)
	}

	window.MakeContextCurrent()
	// vsync
	glfw.SwapInterval(1)

	return window
}

// initOpenGL initializes OpenGL and returns an intiialized program.
func initOpenGL() uint32 {

	if err := gl.Init(); err != nil {
		panic(err)
	}

	version := gl.GoStr(gl.GetString(gl.VERSION))
	log.Println("OpenGL version", version)

	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}

	prog := gl.CreateProgram()
	gl.AttachShader(prog, vertexShader)
	gl.AttachShader(prog, fragmentShader)
	gl.LinkProgram(prog)

	return prog
}
