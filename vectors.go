package main

import (
	"github.com/barnex/fmath"
)

type Vector2 struct {
	x float32
	y float32
}

type Vector3 struct {
	x float32
	y float32
	z float32
}

type Vector4 struct {
	x float32
	y float32
	z float32
	a float32
}

func (vector *Vector2) add(value Vector2) {
	vector.x += value.x
	vector.y += value.y
}

func (vector *Vector3) add(value Vector3) {
	vector.x += value.x
	vector.y += value.y
	vector.z += value.z
}

func (v *Vector3) subtract(value Vector3) {
	v.x -= value.x
	v.y -= value.y
	v.z -= value.z
}

func (v *Vector3) dot(value Vector3) float32 {
	return v.x*value.x + v.y*value.y
}

func (v *Vector3) mult(value float32) {
	v.x *= value
	v.y *= value
	v.z *= value
}

func (v *Vector3) normalize() {
	v.mult(1.0 / fmath.Sqrtf(v.x*v.x+v.y*v.y+v.z*v.z))
}
