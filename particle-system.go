package main

import (
	"math/rand"

	"github.com/barnex/fmath"
)

func randFloats(min, max float32) float32 {
	return min + rand.Float32()*(max-min)
}

// PointForce ...
type PointForce struct {
	position             Vector3
	strength             float32
	linearAttenuation    float32 // линейное ослабление
	quadraticAttenuation float32 // квадратное ослабление
}

// ParticleSystem ...
type ParticleSystem struct {
	particles        []Particle
	particleCredit   float32 // начальное число частить TODO: rename variable
	origin           Vector3
	pointForces      []PointForce
	directionalForce Vector3
	spawnRate        float32
	particleSpeedMin float32
	particleSpeedMax float32
	particleSizeMin  float32
	particleSizeMax  float32
	particlelifeMin  float32
	particlelifeMax  float32
	frictionFactor   float32 // вроде это противодействуящая сила
	rotate           bool
	colorTheme       [12]Vector4
	time             float32
}

func (ps *ParticleSystem) setColorScheme(colorTheme string) {
	if colorTheme == "fire" {
		for i := 0; i < 4; i++ {
			ps.colorTheme[i] = Vector4{float32(i / 4), 0, 0, 0}
			ps.colorTheme[i+4] = Vector4{1, float32(i / 4), 0, 0}
			ps.colorTheme[i+8] = Vector4{float32((3 - i) / 3), float32((3 - i) / 3), 1, 0}
		}
	}
}

func (ps *ParticleSystem) addParticle() {
	position := ps.origin
	direction := Vector3{randFloats(0, 0.3), 1, randFloats(0, 0.3)}
	direction.normalize()
	direction.mult(randFloats(ps.particleSpeedMin, ps.particleSpeedMax))
	var size float32 = randFloats(ps.particleSizeMin, ps.particleSizeMax)
	liveSpan := randFloats(ps.particlelifeMin, ps.particlelifeMax)
	var invInitialLife float32 = 1.0 / liveSpan

	particle := Particle{position, direction, liveSpan, invInitialLife, size, 0, 0, 0}
	ps.particles = append(ps.particles, particle)
}

func (ps *ParticleSystem) update(timestamp float32) {
	deltaTime := ps.time - timestamp
	ps.time = timestamp

	ps.particleCredit += ps.time + ps.spawnRate
	length := int(ps.particleCredit)
	ps.particleCredit -= float32(length)

	for i := 0; i < length; i++ {
		ps.addParticle()
	}

	frictions := fmath.Pow(ps.frictionFactor, deltaTime)

	for i := 0; i < len(ps.particles); i++ {
		if ps.particles[i].isDead(deltaTime) {
			ps.particles = ps.particles[:len(ps.particles)-1]
			continue
		}

		vector := Vector3{0, 0, 0}
		for j := 0; j < len(ps.pointForces); j++ {
			direction := ps.pointForces[j].position
			direction.subtract(ps.particles[i].position)

			distance := direction.dot(direction)
			// optimize formula
			direction.mult(ps.pointForces[j].strength / (1.0 + fmath.Sqrtf(distance)*ps.pointForces[j].linearAttenuation + distance*ps.pointForces[j].quadraticAttenuation))
			vector.add(direction)
		}

		ps.directionalForce.add(vector)
		ps.particles[i].direction.add(ps.directionalForce)
		ps.particles[i].direction.mult(deltaTime)
		ps.particles[i].direction.mult(frictions)

		if ps.rotate {
			ps.particles[i].angle += ps.particles[i].angleSpeed * deltaTime
		}

		ps.particles[i].update(deltaTime)
	}

}
